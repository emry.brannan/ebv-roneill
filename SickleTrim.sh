#!/bin/bash
#SBATCH --job-name=sickle_trim_smRNA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=emry.brannan@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load sickle/1.33
module load fastqc/0.11.7
module load fastx/0.0.13

fileList="*fastq"
inPATH="/home/FCAM/ebrannan/"
outPATH="/home/FCAM/ebrannan/~sickle/"

for file in ${fileList}
do

        prefix=$(echo ${file} | cut -d "." -f 1)

     #filter based on quality
        sickle se -f ${inPATH}${file} \
        -t sanger -o ${outPATH}${prefix}_sickle.fastq -q 30 -l 15
        echo ${file} sickle is DONE!

        fastqc -o ${outPATH} ${outPATH}${prefix}_sickle.fastq
        echo ${prefix}_sickle.fastq fastqc is DONE!

     #trim bases off the 3' end
        fastx_trimmer -Q33 -l 71 \
        -i ${outPATH}${prefix}_sickle.fastq \
        -o ${outPATH}${prefix}_sickle_trim.fastq
        echo ${prefix}_sickle_trim.fastq fastx_trimmer is DONE!

        fastqc -o ${outPATH} ${outPATH}${prefix}_sickle_trim.fastq
        echo ${prefix}_sickle_trim.fastq fastqc is DONE!

done
