# EBV-RONeill
original data located here `/labs/Oneill/jules/`
The analysis is being performed at the location `/labs/Oneill/jules/ebrannan/`

There are two data sets in this project; the first is a total library RNA 
and the second is a small RNA library.

First, I retrieved the data from xanadu: > `/labs/Oneill/jules. `

Then I combined the files.
And ran a FastQ on the combined data
```
> `fileList="*combined.fastq.gz"`
> inPATH="/labs/Oneill/jules/single_end_fastas/"
outPATH="/labs/Oneill/jules/ebrannan/"
```

>



