#!/bin/bash
#SBATCH --job-name=eb_fastqc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=emry.brannan@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

fileList="*.fastqc"
inPATH="/home/FCAM/ebrannan/"
outPATH="/labs/Oneill/jules/ebrannan/"

module load fastqc

for file in ${fileList}
do
     fastqc -o ${outPATH} ${inPATH}${file}
        echo ${file} fastqc DONE!
done
