#Named fastxlip_HSA.sh
#/home/FCAM/ebrannan

#!/bin/bash
#SBATCH --job-name=fastxclip_smRNA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=emry.brannan@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

fileList= "*.fastq"
inPATH="/labs/Oneill/jules/combined_single_end_fastas/"
outPATH="/home/FCAM/ebrannan/"

#load modules
module load fastx/0.0.13
module load fastqc/0.11.7

for file in ${fileList}
do

	prefix=$(echo ${file} | cut -d "." -f 1)

	#remove illumina smRNA 3prime adapter from each file in fileList
	fastx_clipper -Q33 -a TGGAATTCTCGGGTGCCAAGG -i ${inPATH}${file} -o ${outPATH}${prefix}_fastxclip.fastq
	echo ${file} smRNA 3prime adapter clip DONE!

	#fastqc quality check
	fastqc -o ${outPATH} ${outPATH}${prefix}_fastxclip.fastq
	echo ${prefix}_fastxclip.fastq fastqc DONE!

done
