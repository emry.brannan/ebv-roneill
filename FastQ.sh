#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=emry.brannan@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


fileList="*.fastq"
inPATH="/labs/Oneill/jules/combined_single_end_fastas/"
outPATH="/home/FCAM/ebrannan/"

module load fastqc

for file in ${fileList}
do

     prefix=$(echo ${file} | cut -d "." -f 1)

     fastqc -o ${outPATH} ${inPATH}${file}
	echo ${prefix} fastqc DONE!

done
